package com.voxloud.provisioning.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.voxloud.provisioning.entity.Device;
import com.voxloud.provisioning.repository.DeviceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Service
@Slf4j
@Configuration
@PropertySource(ignoreResourceNotFound = true, value = "classpath:application.properties")
public class ProvisioningServiceImpl implements ProvisioningService {

 @Value("${provisioning.domain}")
    private String domain;

    @Value("${provisioning.port}")
    private String port;

    @Value("${provisioning.codecs}")
    private String codecs;

    @Value("${provisioning.desk.path}")
    private String deskPath;

    @Autowired
    DeviceRepository deviceRepository;

    public String getProvisioningFile(String macAddress) {
    Device device=deviceRepository.findByMacAddress(macAddress);
       try {
           if (device != null) {
           log.info("Device Found");
                if("CONFERENCE".equals(device.getModel().name())){
                    log.info("this is Conf");
                    Map<String, String> map = new HashMap<>();
                    map.put("username",device.getUsername());
                    map.put("password",device.getPassword());
                    if(device.getOverrideFragment()==null) {
                        map.put("domain", domain);
                        map.put("port", port);
                        String[] codec = codecs.split(",");
                        map.put("codecs", Arrays.asList(codec) + "");
                    }
                    else{
                        log.info("override"+device.getOverrideFragment());
                        TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {};
                        Map<String, String> overrideMap = new ObjectMapper().readValue(device.getOverrideFragment(), typeRef);
                        map.putAll(overrideMap);
                    }
                    return new ResponseEntity<>(map, HttpStatus.OK);
                }
                else{
                    log.info("This is Desk");
                    Properties props = new Properties();
                    props.put("username",device.getUsername());
                    props.put("password",device.getPassword());
                    if(device.getOverrideFragment()==null) {
                        props.put("domain", domain);
                        props.put("port", port);
                        props.put("codecs", codecs);
                    }
                    else{
                        log.info("override"+device.getOverrideFragment());
                        Properties p = new Properties();
                        p.load(new StringReader(device.getOverrideFragment()));
                        for (Map.Entry<Object, Object> entry : p.entrySet()) {
                            props.put(entry.getKey(), entry.getValue());
                        }
                    }
                    String path = deskPath+"\\"+macAddress+".properties";
                    File file = ResourceUtils.getFile(path);
                    Path filePath = Paths.get(file.getPath());
                    FileOutputStream outputStrem = new FileOutputStream(path);
                    props.store(outputStrem, "This is "+ device.getModel().name()+" properties file");
                    UrlResource res = new UrlResource(filePath.toUri());
                    return ResponseEntity.ok()
                            .body(res);
               }
           }
           else {
               log.info("Device Not Found");
               return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
           }
       }
       catch (Exception e){
                log.error(e.getMessage());
       }
       return null;
    }
}
