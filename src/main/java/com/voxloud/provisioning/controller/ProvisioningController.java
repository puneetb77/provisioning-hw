package com.voxloud.provisioning.controller;

import com.voxloud.provisioning.entity.Device;
import com.voxloud.provisioning.repository.DeviceRepository;
import com.voxloud.provisioning.service.ProvisioningService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
@Slf4j
public class ProvisioningController {

    @Autowired
    ProvisioningService provisioningService;

    @Autowired
    DeviceRepository deviceRepository;
    @GetMapping("/provisioning/{mac}")
    public ResponseEntity<?> getProvisioningFile(@PathVariable("mac") String macAdd){
        log.info("Inside the get Provisioning File");
        return provisioningService.getProvisioningFile(macAdd);
    }
    
     @PostMapping("/provisioning")
    public Device createProvisioningFile(@RequestBody Device device){
        device.setMacAddress(getRandomMacAddress());
        log.info("Inside the create Provisioning File");
        return deviceRepository.save(device);

    }
    
    public static String getRandomMacAddress() {
        String mac = "";
        Random r = new Random();
        for (int i = 0; i < 6; i++) {
            int n = r.nextInt(255);
            mac += String.format("%02x", n);
        }
        return mac.toUpperCase();
    }
    // TODO Implement controller method
}